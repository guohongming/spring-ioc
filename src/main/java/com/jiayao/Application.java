package com.jiayao;

import com.jiayao.service.CustomerService;
import com.jiayao.service.UserService;
import context.AnnotationConfigApplicationContext;

import java.util.Set;

/**
 * 类 名: Application
 * 描 述:
 * @author: jiaYao
 */
public class Application {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.jiayao");
        System.out.println("---------------容器创建完毕-------------------");
        // 测试类型首字母小写获取
        UserService userService = (UserService) context.getBean("userService");
        System.out.println(userService);
        // 测试自定义名称
        CustomerService customerService = (CustomerService) context.getBean("myCust");
        System.out.println(customerService);
        // 测试懒加载
        Object lazyService = context.getBean("lazyService");
        System.out.println(lazyService);
        // 测试循环依赖
        System.out.println("测试循环依赖  " + (customerService == userService.getMyCust()));
        System.out.println("--------------------------------------------------------");
        Set<String> beanDefinitionNames = context.getBeanDefinitionNames();
        beanDefinitionNames.forEach(System.out::println);
    }

}
