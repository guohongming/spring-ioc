package com.jiayao.service;

import core.annotation.MyAutowired;
import core.annotation.MyService;

/**
 * 类 名: CustomerService
 * 描 述:
 * 作 者: 黄加耀
 * 创 建: 2019/12/5 : 23:55
 * 邮 箱: huangjy19940202@gmail.com
 *
 * @author: jiaYao
 */
@MyService( name = "myCust")
public class CustomerService {

    @MyAutowired
    private UserService userService;

}
