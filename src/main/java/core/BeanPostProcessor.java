package core;

/**
 * Bean的后置处理器
 */
public interface BeanPostProcessor {

	/**
	 * Bean的初始化前置处理
	 * @param bean
	 * @param beanName
	 * @return
	 */
	public Object postProcessBeforeInitialization(Object bean, String beanName);

	/**
	 * Bean的初始化的后置处理
	 * @param bean
	 * @param beanName
	 * @return
	 */
	public Object postProcessAfterInitialization(Object bean, String beanName);

}
