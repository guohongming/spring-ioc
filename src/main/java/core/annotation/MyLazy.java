package core.annotation;

import java.lang.annotation.*;

/**
 * 类 名: MyLazy
 * 描 述:
 * 当添加了这个注解，则表示默认是懒加载的
 * @author: jiaYao
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@MyComponent
public @interface MyLazy {
}
