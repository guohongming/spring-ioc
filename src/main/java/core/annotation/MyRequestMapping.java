package core.annotation;

import java.lang.annotation.*;

/**
 * 访问控制层url处理
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MyRequestMapping {

    String value() default "/";

}
