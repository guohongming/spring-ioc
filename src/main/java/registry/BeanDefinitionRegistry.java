package registry;

import core.BeanDefinition;

import java.util.Properties;
import java.util.Set;

/**
 * 类 名: BeanDefinitionRegistry
 * 描 述: bean的注册器
 * @author: jiaYao
 */
public interface BeanDefinitionRegistry {

    /**
     * 关键 -> 往注册表中注册一个新的 BeanDefinition 实例
	 */
    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition);

    /**
     * 从注册中取得指定的 BeanDefinition 实例
	 */
    BeanDefinition getBeanDefinition(String beanName);

    /**
     * 判断 BeanDefinition 实例是否在注册表中（是否注册）
     */
    boolean containsBeanDefinition(String beanName);
    /**
     * 取得注册表中所有 BeanDefinition 实例的 beanName（标识
     */
    Set<String> getBeanDefinitionNames();

    /**
     * 注册配置文件
     * @param properties
     */
    void registerProperties(Properties properties);
}
