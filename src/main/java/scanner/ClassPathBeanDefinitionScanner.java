package scanner;

import com.jiayao.service.CustomerController;
import constant.IocConstant;
import core.BeanDefinition;
import core.annotation.*;
import org.apache.commons.lang3.StringUtils;
import registry.BeanDefinitionRegistry;

import javax.annotation.*;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.*;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.HashSet;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

/**
 * 类 名: ClassPathBeanDefinitionScanner
 * 描 述: 类扫描器
 *
 * @author 52874
 */
public class ClassPathBeanDefinitionScanner {

    /**
     * 注册表 注册
     */
    private final BeanDefinitionRegistry registry;

    /**
     * 扫描包下所有的类的字节码对象(候选资源和非候选资源)
     *
     * @param registry
     */
    private final static Set<String> basePackageClassName = new HashSet<>();
    /**
     * 存储所有的候选资源 的名称
     */
    private final static Set<String> beanDefinitionNames = new HashSet<>();

    public ClassPathBeanDefinitionScanner(BeanDefinitionRegistry registry) {
        this.registry = registry;
    }

    public void scan(String[] basePackages) {
        // 开始进行包扫描
        doScan(basePackages);
    }

    /**
     * 获取所有的候选资源源数据信息
     *
     * @param basePackages
     */
    private void doScan(String[] basePackages) {
        Set<BeanDefinition> candidates = new HashSet<>();
        // 扫描配置文件
        Properties properties = findAllConfigurationFile();
        registry.registerProperties(properties);
        //扫描所有的候选资源列表
        findCandidateComponents(basePackages, candidates);
        // 将扫描出来的候选资源信息 添加到注册表中
        candidates.forEach(beanDefinition -> {
            registry.registerBeanDefinition(beanDefinition.getBeanName(), beanDefinition);
        });
    }

    /**
     * 扫描配置文件
     */
    private Properties findAllConfigurationFile() {
        Properties properties = new Properties();
        try {
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("application.properties");
            if (!Objects.isNull(is)) {
                InputStreamReader inputStreamReader = new InputStreamReader(is, "GBK");
                properties.load(inputStreamReader);
            }
            if (properties.containsKey(IocConstant.active) && !Objects.isNull(properties.get(IocConstant.active))) {
                InputStream is1 = this.getClass().getClassLoader().getResourceAsStream("application-" + properties.get(IocConstant.active) + ".properties");
                if (!Objects.isNull(is1)) {
                    InputStreamReader inputStreamReader = new InputStreamReader(is1, "GBK");
                    properties.load(inputStreamReader);
                }
            }
            if (properties.containsKey(IocConstant.include) && !Objects.isNull(properties.get(IocConstant.include))){
                InputStream is1 = this.getClass().getClassLoader().getResourceAsStream("application-" + properties.get(IocConstant.include) + ".properties");
                if (!Objects.isNull(is1)) {
                    InputStreamReader inputStreamReader = new InputStreamReader(is1, "GBK");
                    properties.load(inputStreamReader);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return properties;
    }

    /**
     * 获取所有的候选资源源数据信息
     *
     * @param basePackages 需要扫描的包的候选资源信息
     * @return
     */
    private void findCandidateComponents(String[] basePackages, Set<BeanDefinition> candidates) {
        for (String basePackage : basePackages) {
            //加载所有class
            loadClass(basePackage);
        }
        // 获取到需要扫描的包路径下所有的类后开始挑选出需要 所有的候选资源信息 BeanDefinition
        basePackageClassName.forEach(packageClassName -> {
            try {
                Class<?> packageClass = Class.forName(packageClassName);
                // 判断是否是候选资源 携带指定注解 @MyComponent
                Class<?> annos = getAnnos(packageClass);
                if (!Objects.isNull(annos)) {
                    String beanName = "";
                    if (annos.getTypeName().equals(MyComponent.class.getTypeName()) && StringUtils.isNotEmpty(packageClass.getAnnotation(MyComponent.class).name())) {
                        beanName = packageClass.getAnnotation(MyComponent.class).name();
                    } else if (annos.getTypeName().equals(MyService.class.getTypeName()) && StringUtils.isNotEmpty(packageClass.getAnnotation(MyService.class).name())) {
                        beanName = packageClass.getAnnotation(MyService.class).name();
                    } else {
                        beanName = toLowercaseIndex(packageClass.getSimpleName());
                    }
                    if (beanDefinitionNames.contains(beanName)) {
                        throw new RuntimeException("beanName已经存在：" + beanName);
                    }
                    boolean isAbstract = false;
                    // 判断当前类是否是抽象的
                    if (Modifier.isAbstract(packageClass.getModifiers())) {
                        isAbstract = true;
                    }
                    // 判断是否是懒加载的
                    if (packageClass.isAnnotationPresent(MyLazy.class)) {
                        candidates.add(new BeanDefinition(beanName, packageClass, packageClassName, true, isAbstract));
                    } else {
                        candidates.add(new BeanDefinition(beanName, packageClass, packageClassName, isAbstract));
                    }
                    beanDefinitionNames.add(beanName);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
    }

    public static void main(String[] args) {
        Class<CustomerController> customerControllerClass = CustomerController.class;
        Class<?> annos = getAnnos(customerControllerClass);
        System.out.println(annos.getSimpleName().equals(MyController.class.getSimpleName()));
        System.out.println(annos.getName());
        System.out.println(annos.getTypeParameters());
        System.out.println(annos.getTypeName());

    }


    /**
     * 判断当前类是否包含组合注解 @MyComponent
     * interface java.lang.annotation.Documented 等 存在循环，导致内存溢出，所以需要排除java的源注解
     *
     * @param classz
     */
    private static Class<?> getAnnos(Class<?> classz) {
        Annotation[] annotations = classz.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType() != Deprecated.class &&
                    annotation.annotationType() != SuppressWarnings.class &&
                    annotation.annotationType() != Override.class &&
                    annotation.annotationType() != PostConstruct.class &&
                    annotation.annotationType() != PreDestroy.class &&
                    annotation.annotationType() != Resource.class &&
                    annotation.annotationType() != Resources.class &&
                    annotation.annotationType() != Generated.class &&
                    annotation.annotationType() != Target.class &&
                    annotation.annotationType() != Retention.class &&
                    annotation.annotationType() != Documented.class &&
                    annotation.annotationType() != Inherited.class &&
                    annotation.annotationType() != MyRequestMapping.class
                    ) {
                if (annotation.annotationType() == MyComponent.class) {
                    return classz;
                } else {
                    return getAnnos(annotation.annotationType());
                }
            }
        }
        return null;
    }

    /**
     * 加载所有的候选资源信息
     *
     * @param basePackage
     */
    private void loadClass(String basePackage) {
        URL url = this.getClass().getClassLoader().getResource(basePackage.replaceAll("\\.", "/"));
        File file = new File(url.getFile());
        if (file.exists() && file.isDirectory()) {
            File[] files = file.listFiles();
            for (File fileSon : files) {
                if (fileSon.isDirectory()) {
                    // 递归扫描
                    loadClass(basePackage + "/" + fileSon.getName());
                } else {
                    // 是文件并且是以 .class结尾
                    if (fileSon.getName().endsWith(".class")) {
                        String beanReferenceName = basePackage.replace("/", ".") + "." + fileSon.getName().replaceAll(".class", "");
                        System.out.println("正在读取class文件： " + beanReferenceName);
                        basePackageClassName.add(beanReferenceName);
                    }
                }
            }
        } else {
            throw new RuntimeException("没有找到需要扫描的文件目录");
        }
    }


    /**
     * @author: JiaYao
     * @demand: 类名首字母转小写
     * @parameters:
     * @creationDate：
     * @email: huangjy19940202@gmail.com
     */
    public static String toLowercaseIndex(String name) {
        if (StringUtils.isNotEmpty(name)) {
            return name.substring(0, 1).toLowerCase() + name.substring(1, name.length());
        }
        return name;
    }
}
